<?php

   // Information de connexion à la DB
   $servername = 'localhost';
   $username = 'dev';
   $password = 'dev';
   
   class Db
   {

      // Connection
      private $connexion;

      // Choix de la table
      private $table;

      private $db;
      private $servername;
      private $username; 
      private $password; 

      // Db connection
      public function __construct($servername,$table, $username,$password)
      {
            $this->table = $table;
            $this->servername = $servername;
            $this->username = $username;
            $this->password = $password;
            $db = new PDO("mysql:host=$this->servername;dbname=$this->table", $this->username, $this->password);
            $this->connexion = $db;
      }

      // Insert dans la db un nouveau produit
      public function insert($produit)
      {
         $sql = $this->connexion->query("INSERT INTO produits (titre, categorie, prix, contact) VALUES ($produit->titre,$produit->categorie,$produit->prix,$produit->contact)");
      }

      // récupère et affiche les produits
      public function afficheAll()
      {
         $sql = $this->connexion->query("SELECT * FROM produits", PDO::FETCH_ASSOC);
         foreach($sql as $value)
         {
            $data[] = $value;
         }
         return json_encode($data);
      }

      // récupère et affiche les produits
      public function afficheOne($id)
      {
         $sql = $this->connexion->query("SELECT * FROM produits WHERE id = $id ", PDO::FETCH_ASSOC);
         foreach($sql as $key => $value)
         {
           return $value = json_encode($value);
         }
      }

      // Affiche les produits compris entre la valeur min et max
      public function affichePrix($min, $max)
      { 
            $sql = $this->connexion->query("SELECT * FROM produits WHERE prix BETWEEN $min AND $max", PDO::FETCH_ASSOC);
            foreach($sql as $value)
            {
               $data[] = $value;
            }
         return json_encode($data);
      }

      // récupère et affiche les produits
      public function deleteOne($id)
      {
         $sql = $this->connexion->query("DELETE * FROM produits WHERE id = $id ", PDO::FETCH_ASSOC);

         return header('Location: /API');
      }
   }